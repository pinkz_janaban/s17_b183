//console.log("Hello World");

let students = [];
let sectionedStudents = [];

function addStudent(name){
	students.push(name);
	console.log(name + " student name has been added.");
}


function countStudents(){
	console.log("The total count of enrolled students are " + students.length)
}

function printStudents(){
	students.sort();
	students.forEach(function(students) {
		console.log(students);
	})
}

function findStudent(name) {
    let match = students.filter(function(student) {
        return student.toLowerCase().includes(name.toLowerCase());
    });
    if (match.length == 1) {
        console.log(match[0] + " is an enrollee.");
    } else if (match.length > 1) {
        console.log("Student has multiple entries");
    } else {
        console.log("No students match this keyword.");
    }
}


